const mysql = require("mysql");

const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "node1234",
  database: "cat_cafe_lab",
});

connection.connect(function (err){
 if (err){
     console.log(err);
 } else{
     console.log("MySQL database is connected");
 }
});

module.exports = connection;