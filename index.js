const express = require("express");
const cors = require("cors");
const db = require("./database/connection");
const app = express();
const bodyParser = require("body-parser");

app.use(cors());
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
 
// parse application/json
app.use(bodyParser.json())

app.get("/", function (req, res) {
  res.send("Hello World");
});

app.get("/api/cats", function (req, res) {
  db.query("SELECT * FROM cats", function (error, results, fields) {
    if (error) throw error;
    return res.status(200).send(results);
  });
});

app.post("/api/cats", (req, res) => {
  // return res.status(201).send(req.body);
  db.query(
    "INSERT INTO cats (name, image) VALUES (?, ?)",
    [req.body.name, req.body.image],
    function (error, results, fields) {
      if (error) throw error;
      return res.status(201).send(results);
    }
  );
});

app.put("/api/cats/:id", (req, res) => {
  const { name, image } = req.body;
  db.query(
    `UPDATE cats SET name="${name}", image="${image}" WHERE id=${req.params.id}`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});

app.delete("/api/cats/:id", (req, res) => {
  db.query(
    `DELETE FROM cats WHERE id=${req.params.id}`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});

//one cat
app.get("/api/cats/:catid", (req, res) => {
  db.query(
    `SELECT * FROM cats WHERE id=${req.params.catid}`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});

const PORT = 3001;
app.listen(PORT, () => console.log(`Server started on port 3001...`));
